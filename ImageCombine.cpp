// ImageCombine.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <fstream>
#include "TexturePacker.h"

#ifdef WIN32 

#define PNG_READ_16_TO_8_SUPPORTED
#include <png.hpp>
#include <windows.h>
#include <tchar.h> 
#include <strsafe.h>
#include <direct.h>
#define GetCurrentDir _getcwd

#else
#include <dirent.h>
#include <pthread.h>
#include <png++/png.hpp>
#endif

using namespace std;

class TextureInformation
{
public:
	TextureInformation(size_t index, std::string path, std::string name)
	: idx(index), image_path(path), image_name(name)
	{
		image = new png::image<png::rgba_pixel>(image_path + image_name,
				png::convert_color_space<png::rgba_pixel>());
	}

	~TextureInformation(void)
	{
		delete image;
	}

	void setIndex(size_t i) { idx = i; }
	size_t getIndex() const { return idx; }
	png::image<png::rgba_pixel>* getImage() const { return image; }
	std::string getImageName() {return image_name;}

	void writeImage(png::image< png::rgba_pixel > &output_image,
			std::string output_image_name,
			const size_t offset_x, const size_t offset_y,
			const size_t out_width, const size_t out_height)
	{
		for(size_t out_y = offset_y; out_y < (offset_y + out_height); ++out_y)
		{
			for(size_t out_x = offset_x; out_x < (offset_x + out_width); ++out_x)
			{
				size_t rot_y = out_y - offset_y;
				size_t rot_x = out_x - offset_x;
				output_image[out_y][out_x] = (*image)[rot_y][rot_x];
			}
		}
		output_image.write(output_image_name);
	}


private:
	size_t                           idx;
	std::string                   image_path;
	std::string 				  image_name;
	png::image<png::rgba_pixel>*  image;
};

class TextureAtlas
{
public:
	TextureAtlas(size_t im_count)
	: max_atlas_width(1024), max_atlas_height(1024)
	{
		atlas = TEXTURE_PACKER::createTexturePacker();
		atlas->setTextureCount(im_count);
	}

	~TextureAtlas(void)
	{
		delete(atlas);
		for(std::list<TextureInformation *>::iterator images_iter = images.begin();
				images_iter != images.end();images_iter++)
		{
			delete *images_iter;
		}
	}

	bool addTexture(TextureInformation* image_info)
	{
		png::image<png::rgba_pixel>* image = image_info->getImage();
		atlas->addTexture(image->get_width(), image->get_height());
		image_info->setIndex(atlas->getTextureCount() - 1);
		images.push_back(image_info);
		return true;
	}

	void packTextures(void)
	{
		int w, h;
		atlas->packTextures(w, h, true, false);
		max_atlas_width = w;
		max_atlas_height = h;
	}

	size_t packedWidth(void) { return max_atlas_width; }
	size_t packedHeight(void) { return max_atlas_height; }
	void   setOutputImageName(std::string n) { output_image_name = n; }
	std::string getOutputImageName(void) { return output_image_name; }
	void   setOutputFileName(std::string n) { output_file_name = n; }
	std::string getOutputFileName(void) { return output_file_name; }

	void prepareImage (){
		output_image_name = this->getOutputImageName();

		output_image = new png::image<png::rgba_pixel>(this->packedWidth(),
				this->packedHeight());

		std::ofstream text_file_stream;
		char *c_output_file_name = (char*)output_file_name.c_str();

		text_file_stream.open(c_output_file_name);

		for(std::list<TextureInformation *>::iterator images_iter = images.begin();
				images_iter != images.end();
				images_iter++)
		{
			TextureInformation* image_info = *images_iter;
			int x, y, width, height;
			atlas->getTextureLocation(image_info->getIndex(),
					x, y, width, height);

			text_file_stream << "Image name : " << image_info->getImageName() << endl;
			text_file_stream << "Image x-cord : " << x << endl;
			text_file_stream << "Image y-cord : " << y << endl;
			text_file_stream << "Image width : " << width << endl;
			text_file_stream << "Image height : " << height << endl << endl;
			image_info->writeImage(*output_image, output_image_name, x, y, width, height);
		}
		text_file_stream.close();
		cout << "Texture atlas saved : " << output_image_name << endl << endl;
		cout << "Descriptor file saved : " << c_output_file_name << endl;
	}

private:
	size_t                           max_atlas_width;
	size_t                           max_atlas_height;
	TEXTURE_PACKER::TexturePacker*   atlas;
	std::list<TextureInformation *>  images;
	png::image<png::rgba_pixel>*     output_image;
	std::string                      output_image_name;
	std::string                      output_file_name;
};

int main(int argc, char* argv[])
{
	std::string image_name;
	std::vector<std::string> image_names;
	char* folder_location = argv[1];
	std::string folder_location_str (folder_location);
	std::list<TextureAtlas *> atlases;
	std::string current_directory_str;

#if WIN32

	WIN32_FIND_DATA ffd;
	TCHAR szDir[MAX_PATH];
	size_t length_of_arg;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	LARGE_INTEGER filesize;
	DWORD dwError=0;
	DWORD directoryLength = 0;

	if(argc != 2)
	{
		_tprintf(TEXT("\nUsage: %s <directory name>\n"), argv[0]);
		return (-1);
	}
	StringCbLength(folder_location, MAX_PATH, &length_of_arg);

	if (length_of_arg > (MAX_PATH - 3))
	{
		_tprintf(TEXT("\nDirectory path is too long.\n"));
		return (-1);
	}

	_tprintf(TEXT("\nImage folder is : %s\n"), argv[1]);
	StringCbCopy(szDir, MAX_PATH, argv[1]);
	StringCbCat(szDir, MAX_PATH, TEXT("\\*"));

	hFind = FindFirstFile(szDir, &ffd);

	do
	{
	if (! (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			filesize.LowPart = ffd.nFileSizeLow;
			filesize.HighPart = ffd.nFileSizeHigh;
			image_name = ffd.cFileName;

			image_names.push_back (image_name);
		}
	}
	while (FindNextFile(hFind, &ffd) != 0);

	FindClose(hFind);

	char cDirectoryPath[FILENAME_MAX];

	directoryLength = GetCurrentDirectory (sizeof(cDirectoryPath),cDirectoryPath );
	current_directory_str.assign(cDirectoryPath,directoryLength);
	current_directory_str = current_directory_str + "\\";

#else

	DIR *pDIR;
	struct dirent *entry;

	if( pDIR=opendir(folder_location) ){
		while(entry = readdir(pDIR)){
			if( strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0 ) {
				image_name = entry->d_name;
				image_names.push_back(image_name);
			}
		}
		closedir(pDIR);
	}

#endif
	cout << endl;
	size_t idx;
	// create the initial texture
	atlases.push_back(new TextureAtlas( image_names.size()));
	//	load the texture

	for (idx = 0; idx < image_names.size(); ++idx) {

		//load the image
		TextureInformation *ti = new TextureInformation (idx, folder_location_str, image_names[idx]);
		for(std::list<TextureAtlas *>::iterator atlases_iter = atlases.begin();
				atlases_iter != atlases.end();
				atlases_iter++)
		{
			TextureAtlas* tai = *atlases_iter;
			if (tai->addTexture(ti))
				break;

		}
	}

	for(std::list<TextureAtlas *>::iterator atlases_iter = atlases.begin();
			atlases_iter != atlases.end();
			atlases_iter++)
	{
		TextureAtlas* tai = *atlases_iter;

		// Set the atlas name
		tai->setOutputImageName(current_directory_str + "atlas.png");
		tai->setOutputFileName(current_directory_str + "atlas.txt");
		// Pack the atlas
		tai->packTextures();
		tai->prepareImage();
	}
	char cChar;
	std::cin.get(cChar);
}