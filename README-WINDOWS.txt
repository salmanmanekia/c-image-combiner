Prerequisites:
1 : ZLib (http://www.zlib.net/)
2 : LibPng (http://www.libpng.org/pub/png/libpng.html)
3 : png++ (http://www.nongnu.org/pngpp/)
4 : TexturePackure (MIT Licensed and Source file included with the Application)

Prerequisite Information:
The program was built on Visual C++ 2008 Express Edition. Libpng 1.5.12 and zlib 1.2.5 were used as well as png++ revision 123 (latest) from the trunk was used. This version can be found at : http://svn.sv.nongnu.org/svn/pngpp/trunk/.

Package Information (related to Windows):
1 : The package contains 2 source files. One ImageCombine(containing main and developed by me) and the other one is TexturePakcer.
2 : It contains a Window Executable folder which contains.
	a : It contains the Windows executable (.exe) and the libpng15d.dll and zlib1d.dll. These both dlls should be in the same folder as the executable.
	b : It contains an example output; atlas.png (texture atlas) and atlas.txt (description file). These both file correspond to images in PNG folder.
3 : It contains a PNG folder which contains some .png images.
4 : and the README-WINDOWS

How to:
To run the executable go to the command prompt and give the location of .exe 'space' location of the folder where the PNG images are. Forexample after giving the following input :

C:\ImageCombine>C:\ImageCombine\ImageCombine.exe "C:\ImageCombine\PNG\\"

The command prompt will display the output something like :

Image folder is : C:\ImageCombine\PNG\

Texture atlas saved : C:\ImageCombine\atlas.png

Descriptor file saved in : C:\ImageCombine\atlas.txt

and obviously there would be the files created at the above mentioned location. Please note that at the end of the location where PNG images are there are two back-slashes (\\).

Thank You !
